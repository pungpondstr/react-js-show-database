import { useEffect, useState } from 'react';
import { Button, Table, Container } from 'react-bootstrap';
import firebase from '../firebase';

const Home = () => {

    const getDataFunction = async () => {
        const db = await firebase.database();

        await db.ref("chats").on("value", (snap) => {
            let data = [];
            snap.forEach(e => {
                data.push(
                    {
                        id: e.key,
                        rent_id: e.val().rent_id,
                        name: e.val().name,
                        message: e.val().message,
                        time: e.val().time,
                    }
                );
            })
            data.sort(function (a, b) {
                return a.rent_id - b.rent_id;
            });
            setData(data)
        });
    }

    useEffect(() => {
        getDataFunction();
    }, []);

    const [getData, setData] = useState([]);
    return (
        <div>
            <Container>
                <Table striped bordered hover>
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Room id</th>
                            <th>Barber / Member</th>
                            <th>Message</th>
                            <th>Date / Time</th>
                        </tr>
                    </thead>
                    {
                        getData.map((u, i) => {
                            return (
                                <tbody key={i}>
                                    <tr>
                                        <td>{u.id}</td>
                                        <td>{u.rent_id}</td>
                                        <td>{u.name}</td>
                                        <td>{u.message}</td>
                                        <td>{u.time}</td>
                                    </tr>
                                </tbody>
                            )
                        })
                    }
                </Table>
            </Container>
        </div>
    );
}

export default Home;